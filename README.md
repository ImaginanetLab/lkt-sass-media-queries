# Installation

Execute the next command:

```
npm i git+https://gitlab.com/Lekrat/lkt-sass-media-queries.git
```

# Usage

## NPM

```
@import "~lkt-sass-media-queries/src/lkt-sass-media-queries.scss";
```

## Gulp

In your main.scss:

```
@import "./../path/to/../node_modules/lkt-sass-media-queries/src/lkt-sass-media-queries";
```

# Configure

After import in your project, you can add media queries:

```
$add: lkt-media-add(medium, 720px);
$add: lkt-media-add(large, 1280px);
$add: lkt-media-add(small, false, 600px);
$add: lkt-media-add(print, false, false, print);

$add: lkt-media-add-between(sample1, width, 1490px, 1500px);
$add: lkt-media-add-between(sample2, aspect-ratio, 2, 3, print);

$add: lkt-media-add-custom(custom, '(max-width: 177px)');

$add: lkt-media-add-type(braille, braille);
```

lkt-media-add has the following arguments:

- $name: media query name
- $property: watched property
- $from: min-width, false to ignore
- $to: max-width: false to ignore
- $media-type: false to ignore. braille|embossed|handheld|print|projection|screen|speech|tty|tv
- $media-query: custom media query. Must be quoted and a valid media query.

## Functions
| Function | Description | Params |
|---|---|---|
| lkt-media-add | Add a media query. It's a shorthand for lkt-media-add-between ($property = width) | $name, $from, $to, $media-type |
| lkt-media-add-between | Add a media query. | $name, $property, $from, $to, $media-type |
| lkt-media-add-type | Add a media query. | $name, $media-type |
| lkt-media-add-custom | Add a media query. | $name, $media-query |

# Api

Access layers through mixin:

```
.sample
{
    @include lkt-media(large){
        ... your css/sass code ...
    }
}
```

# Examples

This sass:

```
$add: lkt-media-add(sample, 1490px);
$add: lkt-media-add-between(sample2, width, 1490px, 1500px);
$add: lkt-media-add-between(sample3, aspect-ratio, 2, 3, print);
$add: lkt-media-add-custom(custom, '(max-width: 177px)');
$add: lkt-media-add(print, false, false, print);
$add: lkt-media-add-type(braille, braille);

.test{
  @include lkt-media(sample){
    color: red;
  }
  @include lkt-media(sample2){
    color: blue;
  }
  @include lkt-media(sample3){
    color: yellow;
  }
  @include lkt-media(custom){
    color: blue;
  }
  @include lkt-media(print){
    color: purple;
  }
  @include lkt-media(braille){
    color: black;
  }
}

.test2 {
  @include lkt-media(custom) {
    font-size: 10px;

    @include lkt-media(print){
      font-size: 20px;
    }
  }
}
```

will generates the following css:

```
@media screen and (min-width: 1490px) {
  .test {
    color: red;
  }
}

@media screen and (min-width: 1490px) and (max-width: 1500px) {
  .test {
    color: blue;
  }
}

@media print and (min-aspect-ratio: 2) and (max-aspect-ratio: 3) {
  .test {
    color: yellow;
  }
}

@media (max-width: 177px) {
  .test {
    color: blue;
  }
}

@media print {
  .test {
    color: purple;
  }
}

@media braille {
  .test {
    color: black;
  }
}

@media (max-width: 177px) {
  .test2 {
    font-size: 10px;
  }
}

@media print and (max-width: 177px) {
  .test2 {
    font-size: 20px;
  }
}
```